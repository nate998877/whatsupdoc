from django.db import models
from django.contrib.auth.models import User

class Ticket(models.Model):
    title = models.CharField(max_length=280)
    description = models.CharField(max_length=280)
    status = models.CharField(max_length=10,choices=[
        ("new", "New"),
        ("in_progress", "In_Progress"),
        ("done", "Done"),
        ("invalid", "Invalid")
    ], default="new", null=True)
    submitted_by = models.ForeignKey(User, related_name="submitted_by", on_delete=models.CASCADE) #seems like a bad idea to cascade, but i don't have time to figure out a better solution
    assigned_by =  models.ForeignKey(User, related_name="assigned_by" , on_delete=models.CASCADE, default=None, null=True) #seems like a bad idea to cascade, but i don't have time to figure out a better solution
    completed_by = models.ForeignKey(User, related_name="completed_by", on_delete=models.CASCADE, default=None, null=True) #seems like a bad idea to cascade, but i don't have time to figure out a better solution
    time = models.DateTimeField(auto_now_add=True)




"""
When a ticket is assigned, these change as follows:

Status: In Progress
User Assigned: person the ticket now belongs to
User who Completed: None"""

"""
When a ticket is Done, these change as follows:

Status: Done
User Assigned: None
User who Completed: person who the ticket used to belong to"""

"""When a ticket is marked as Invalid, these change as follows:

Status: Invalid
User Assigned: None
User who Completed: None"""