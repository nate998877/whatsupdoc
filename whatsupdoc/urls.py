"""whatsupdoc URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from whatsupdoc import views
from whatsupdoc.models import Ticket


admin.site.register(Ticket)

urlpatterns = [
    path('', views.index, name='homepage'),
    path('admin/', admin.site.urls),
    path('logout/', views.logout_view),
    path('login/', views.login_view),
    path('add-ticket/', views.add_ticket_view),
    path('tickets/', views.tickets),
    path('ticket/<int:key_id>/', views.ticket),
    path('assign-ticket/<int:key_id>/', views.assign_ticket),
    path('complete-ticket/<int:key_id>/', views.complete_ticket),
    path('add-user/', views.add_user),
    path('view-users-tickets/<int:key_id>/', views.users_tickets),
    path('edit/<int:key_id>/', views.edit_view)
]
