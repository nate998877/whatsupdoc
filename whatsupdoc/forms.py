from whatsupdoc.models import Ticket
from django import forms

class Add_Ticket(forms.Form):
    title =  forms.CharField(max_length=280)
    description = forms.CharField(max_length=280)

class LoginForm(forms.Form):
    username = forms.CharField(max_length=50)
    password = forms.CharField(widget=forms.PasswordInput)

class Add_User_Form(forms.Form):
    name = forms.CharField(label='Your name', max_length=100)
    password = forms.CharField(widget=forms.PasswordInput) 

class edit(forms.ModelForm):
    class Meta:
        model = Ticket
        fields = [
            'title',
            'description',
            'status',
            'submitted_by',
            'assigned_by',
            'completed_by',
        ]