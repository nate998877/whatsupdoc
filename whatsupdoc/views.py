from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render, HttpResponseRedirect, reverse
from whatsupdoc.forms import LoginForm, Add_Ticket, Add_User_Form, edit
from whatsupdoc.models import Ticket


@login_required
def add_ticket_view(request):
    if request.method == "POST":
        form = Add_Ticket(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            Ticket.objects.create(
                title=data.get('title'),
                description=data.get('description'),
                submitted_by=request.user,
            )
        return HttpResponseRedirect(reverse('homepage'))

    form = Add_Ticket()
    return render(request, 'generic_form.html', {'form': form})


@login_required
def ticket(request, key_id):
    ticket = Ticket.objects.get(pk=key_id)

    return render(request, 'ticket.html', {"ticket": ticket})


@login_required
def tickets(request):
    tickets = Ticket.objects.all()
    print(request.user)
    return render(request, 'tickets.html', {"tickets": tickets, 'pk': request.user.id})

@login_required
def users_tickets(request, key_id):
    submitted_by = Ticket.objects.filter(submitted_by=key_id)
    assigned_by = Ticket.objects.filter(assigned_by=key_id)
    completed_by = Ticket.objects.filter(completed_by=key_id)

    return render(request, 'users_tickets.html', {
        'submitted_by':submitted_by,
        'assigned_by':assigned_by,
        'completed_by':completed_by,
    })


@login_required
def edit_view(request, key_id):
    if request.method == "POST":
        form = edit(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            t = Ticket.objects.get(pk=key_id)
            t = data
            t.save()
            return HttpResponseRedirect(reverse('homepage'))
    form = edit()

    return render(request, 'generic_form.html', {'form': form})



@login_required
def index(request):
    html = "index.html"
    tickets = Ticket.objects.all()
    new_tickets = filter(lambda x: x.status == "new", tickets)
    in_prog_tickets = filter(lambda x: x.status == "in_progress", tickets)
    completed_tickets = filter(lambda x: x.status == "done", tickets)
    invalid_tickets = filter(lambda x: x.status == "invalid", tickets)

    return render(request, html, {
        "new_tickets": new_tickets,
        "in_prog_tickets": in_prog_tickets,
        "completed_tickets": completed_tickets,
        "invalid_tickets": invalid_tickets,})

@login_required
def assign_ticket(request, key_id):
    ticket = Ticket.objects.get(pk=key_id)
    ticket.status = "in_progress"
    ticket.assigned_by = request.user
    print(ticket)
    ticket.save()
    return HttpResponseRedirect(reverse('homepage'))

@login_required
def complete_ticket(request, key_id):
    ticket = Ticket.objects.get(pk=key_id)
    ticket.status = "done"
    ticket.assigned_by = None
    ticket.completed_by = request.user
    ticket.save()
    return HttpResponseRedirect(reverse('homepage'))


@login_required
def add_user(request):
    if request.method == "POST":
        form = Add_User_Form(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            u = User.objects.create_user(
                username=data['name'],
                password=data['password']
            )
            return HttpResponseRedirect(reverse('homepage'))

    form = Add_User_Form()
    return render(request, 'generic_form.html', {'form': form})

@login_required
def invalid_ticket(request, key_id):
    ticket = Ticket.objects.get(pk=key_id)
    ticket.status = "invalid"
    ticket.assigned_by = None
    ticket.completed_by = None
    print(ticket)
    ticket.save()
    return HttpResponseRedirect(reverse('homepage'))


def login_view(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            if user:= (
                    authenticate(
                        username=data["username"],
                        password=data["password"]
                    )
                ):
                login(request, user)
                return HttpResponseRedirect(
                    request.GET.get('next', reverse('homepage'))
                )

    form = LoginForm()
    return render(request, 'generic_form.html', {'form': form})


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('homepage'))
